---
layout: page
title: Conceptual Work
permalink: /conceptual/
---
# Cult, 2022 #
This work involves the formation of what appears to be a spiritual cult
based upon *The Exegesis of Philip K. Dick* and European paganism.  This includes, but
is not limited to a web presense, social media, literature, clothing, style,
and audio messages.

# Half Earth Inversion, 2021 #
 The work require cleaving the planet in half and inverting the semispheres' convex sides toward on-another permenantly. 

# Five Dimensional Sculpture, 2021 #
  A sculpture approximately the size of a breadbox, but existing in five dimensions.  Being a resident of three-dimensional space-time, I likely cannot fully comprehend a five-dimensional object yet one is required for this piece.

# Sclupture That Moves Backwards In Time, 2020 #
 This piece can be realized through at least two methods.  The first requires the use of time travel by an artist or series of artists.  The second through a series of subtractions in normal time.

# Disassembly LLC, 2018 #
  This work consists of a professional trade-style team of workers who do nothing but disassemble machines, motors, and vehicles via contract.  They bring necessary tools and containers for the collection of liquids and small parts.  Little it no concern for reassembly is given.  Sometimes teardown manuals are consulted.

  The working team works in shifts and takes regular breaks in a designated break area that will furnish coffee, allow smoking of pipes and cigarettes, and have fresh cool water.  This is also where the team meetings occur.

  Work continues as funding and access are maintained as per contract.  A Disassembly Line may be created if a number of similar items require disassembly.
  
  The LLC is for limiting liability.

# Hyperminimal Timepiece, 1999 #
  This object is a silver, unadorned pocket watch with no face, no closure, and no chain.  On its side there is a knob which to wind the mechanism but there is no discernable method for setting the proper time.  If kept wound the watch emits an extrmely mininal ticking sound.