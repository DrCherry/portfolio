#!/bin/bash

for i in images/* ; do
  if [ -f "$i" ]; then

    incam=false
    ingal=false

    #echo --------
    #echo "${i##*/}"
    if grep -q "$i" _galleries/*.md ;
    then
       #echo "galleries yes"
       ingal=true
    fi

    if grep -q "$i" _cameras/*.md ;
    then
       #echo "cameras yes"
       incam=true
    #else
    #   echo "cameras no"
    fi

    if $ingal || $incam ; then
      echo "image is being referenced"
    else
      echo "removing $i"
      rm -f images/$i
    fi

  fi
done

