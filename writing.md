---
layout: page
title: Prose
permalink: /prose/
---
{% assign sp = site.posts %}

{% for p in sp %}

{% if p.display != false %}
**[{{ p.title }}]({{ site.url }}{{ p.url }})** 
 {{p.content}}
{% endif %}

{% endfor %}


