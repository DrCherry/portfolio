---
title: Time Magazine Camera
layout: page
path: /cameras/time/
camera: /cameras/time/
image: /images/IMG_20170717_093039.jpg
description: Promotion and mediation of the spectacle. Countless errors compounding on burning oil fields.
index: 8
textcolor: dark 
images:
  - image: /images/IMG_20170717_093039.jpg
---

{% for item in page.images %}

![ {{ site.url }}{{ item.image }} ]( {{ site.url }}{{ item.image }} )

    
{% endfor %}

{{ page.description }}


#### Galleries featuring the {{ page.title }}

{% assign sg = site.galleries  %}                            {% for g in sg %}

  {% if g.camera=="/cameras/time/"  %} 

  + [{{ g.title }}]({{ site.url }}{{ g.url }})                 {% endif %}

  {% endfor %}


