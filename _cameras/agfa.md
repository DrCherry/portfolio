---
title: Agfa Joslette
layout: page
camera: /cameras/agfa/
image: /images/IMG_20170610_143315.jpg
description: (1937) Lenses locked up tight. Whale oil. Front lens group. Locomotive time table. Standard time.  
index: 100
path: /cameras/agfa/
textcolor: dark
images:
  - image: /images/IMG_20180908_054117_DRO.jpg
  - image: /images/IMG_20170610_143315.jpg
---

{% for item in page.images %}
![ {{ site.url }}{{ item.image }} ]( {{ site.url }}{{ item.image }} )

{% endfor %}

{{ page.description }}


### Galleries featuring the {{ page.title }}               
{% assign sg = site.galleries  %}
  {% for g in sg %}                                        
  {% if g.camera=="/cameras/agfa/"  %}
  + [{{ g.title }}]({{ site.url }}{{ g.url }})
  {% endif %}

  {% endfor %}
