---
title: Arax 60
path: /cameras/arax60/
camera: /cameras/arax60/
layout: page
image: /images/IMG_20170615_140116.jpg
description: Medium Format, 6x6. My life is a falling brick, caught in a bucket, four and a half stories down. 
index: 101
textcolor: dark
images: 
  - image: /images/IMG_20170615_140116.jpg
  
---

{% for item in page.images %}

![ {{ site.url }}{{ item.image }} ]( {{ site.url }}{{ item.image }} )

{% endfor %}

{{ page.description }} 

 
#### Galleries featuring the {{ page.title }}

{% assign sg = site.galleries  %}
  {% for g in sg %}

  {% if g.camera=="/cameras/arax60/"  %}               
  + [{{ g.title }}]({{ site.url }}{{ g.url }})

  {% endif %}

  {% endfor %}


