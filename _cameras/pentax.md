---
title: Pentax Spotmatic
path: /cameras/pentax/
camera: /cameras/pentax/
layout: page
image: /images/IMG_20190309_205505867.jpg
description: (1964-1976) Lifelong road trip, the high water mark, leaded gasoline, invisible dogma, absence.
index: 5
display: true
textcolor: light
images:
  - image: /images/IMG_20190309_205505867.jpg
---

{% for item in page.images %}

![ {{ site.url }}{{ item.image }} ]( {{ site.url }}{{ item.image }} )

    
{% endfor %}
 
### {{ page.title }}
{{ page.description }}

# Galleries featuring the {{ page.title }}

{% assign sg = site.galleries  %}
  {% for g in sg %}

  {% if g.camera=="/cameras/pentax/"  %}

  + [{{ g.title }}]({{ site.url }}{{ g.url }})

  {% endif %}

  {% endfor %}

