---
title: Ansco Pioneer 620
path: /cameras/ansco/
camera: /cameras/ansco/
layout: page
image: /images/IMG_20180829_040408_226.jpg
description: (1947-1953)  Big box, one lever. Big 620. Big 6x9. 
index: 1
textcolor: light 
images:
  - image: /images/IMG_20181231_010148_202.jpg
  - image: /images/09600012.jpg
  - image: /images/09600013.jpg
  - image: /images/09600015.jpg
  - image: /images/31100003.jpg
  - image: /images/31100007.jpg
  - image: /images/31100010.jpg
  - image: /images/31100023.jpg
  - image: /images/31100024.jpg

---

![ {{ site.url }}{{ page.image }} ]( {{ site.url }}{{ page.image }} ) 

{{ page.description }} 

{% for item in page.images %}


{% endfor %}



### Galleries featuring the {{ page.title }}               
{% assign sg = site.galleries  %}
  {% for g in sg %}                                        
  {% if g.camera=="/cameras/ansco/"  %}

  + [{{ g.title }}]({{ site.url }}{{ g.url }})

  {% endif %}

  {% endfor %}
