---
title: Minolta SRT 101
path: /cameras/minolta/ 
camera: /cameras/minolta/
layout: page
image: /images/IMG_20171220_100857264_BURST000_COVER_TOP.jpg
description: Heavy alloy, specific gravity, depleted uranium, factory floors, lunch whistles, trade paper sellout.
index: 999 
textcolor: light
images:
  - image: /images/IMG_20171220_100857264_BURST000_COVER_TOP.jpg

---

{% for item in page.images %}

![ {{ site.url }}{{ item.image }} ]( {{ site.url }}{{ item.image }} )
  

{% endfor %}

{{ page.description }}
  
#### Galleries featuring the {{ page.title }}
 
{% assign sg = site.galleries  %}
  {% for g in sg %}
  {% if g.camera=="/cameras/minolta/"  %}
  + [{{ g.title }}]({{ site.url }}{{ g.url }})
  {% endif %}

  {% endfor %}

