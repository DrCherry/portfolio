---
title: Kodak Bullet 127
path: /cameras/kodak-bullet/
camera: /cameras/kodak-bullet/
layout: page
image: /images/IMG_20170608_140608816.jpg
description: Walter Dorwin Teague, 127, light leaks, a night to remember.
index: 5
textcolor: light
images:
  - image: /images/IMG_20170608_135609066_HDR.jpg
  - image: /images/IMG_20170608_135812764.jpg 
  - image: /images/IMG_20170608_140422202.jpg  
  - image: /images/IMG_20170608_140535368.jpg
  - image: /images/IMG_20170608_140608816.jpg
  - image: /images/IMG_20170608_135535759.jpg

---
  
{{ page.description }} 

{% for item in page.images %}

![ {{ site.url }}{{ item.image }} ]( {{ site.url }}{{ item.image }} ){:width="360px"}

    
{% endfor %} 

![ {{site.url }}{{ page.image }} ]( {{ site.url }}{{ page.image }} )

   
#### Galleries featuring the {{ page.title }}  

{% assign sg = site.galleries  %}
  {% for g in sg %}
  
  {% if g.camera=="/cameras/kodak-bullet/"  %} 

  + [{{ g.title }}]({{ site.url }}{{ g.url }}) 

  {% endif %}  

  {% endfor %}
  
