---
title: Zorki 4
path: /cameras/zorki4/
camera: /cameras/zorki4/
layout: page
image: /images/IMG_20180905_030839.jpg
description: (1957-1959) KMZ, Krasnogorsk, Russia. CCCP. Means of Production. Workers of the World. Iron Curtain. Brutalism. Echos.
index: 3
textcolor: light
images:
  - image: /images/IMG_20180905_030839.jpg

---
 
{{ page.description }}
 
{% for item in page.images %}

![ {{ site.url }}{{ item.image }} ]( {{ site.url }}{{ item.image }} )

{% endfor %}
 
#### Galleries featuring the {{ page.title }}               

{% assign sg = site.galleries  %}
{% for g in sg %}
  {% if g.camera=="/cameras/zorki4/"  %} 

  + [{{ g.title }}]({{ site.url }}{{ g.url }})

  {% endif %}
{% endfor %}
