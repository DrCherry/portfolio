---
title: Polaroid Land Camera 430
layout: page
path: /cameras/land-camera/
camera: /cameras/land-camera/
image: /images/IMG_20170715_141146_1.jpg
description: Apollo missions and launchpad checklists.  New calculus spreading our reach unevenly, wood-clay models and alchemy hurtling into interstellar space.
index: 60
textcolor: light
images:
  - image: /images/IMG_20170715_141146_1.jpg
---

{% for item in page.images %}

![ {{ site.url }}{{ item.image }} ]( {{ site.url }}{{ item.image }} )
    
{% endfor %}

{{ page.description }} 
