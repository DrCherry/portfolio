---
layout: page
title: About
permalink: /about/
index: 10
---
# Apostasy, Arms, Absterge, Accismus #

## Bio ##


Mr. Cherry was born and raised in Lansing, Michigan, where he attended parochial school and was a boy scout. He attended military school in northern Indiana but was expelled at the end of eighth grade. He is an information technologist by trade and currently owns a 1920s storefront in historic Detroit, Michigan with his wife, Akita, and two cats. 

## Publications ##


**[Alter Analog](https://www.alter-analog.com)**, June 2019 - 
 [Steven Cherry revisits 1969](https://www.alter-analog.com/archive/steven-cherry-revisits-1969) 


**[Uncertain Magazine](https://uncertainmag.com/)**, June 2019 -    Issue #5: [ISSUE5](https://issuu.com/uncertainmag/docs/uncertain_issue5)

## Exhibitions ##

**C H E R R I E S ! ! ! ! !**  
July 8th, 2017  
[Public Pool](https://apublicpool.com/), Hamtramck, Michigan  


**Espress/PhotO**  
November 4th, 2017  
Urban Bean, Detroit, Michigan  

**The Regulars**  
January 20, 2018  
[Public Pool](https://apublicpool.com/), Hamtramck, Michigan  

**The Dirty Show 19**  
February 10, 2018  
Russel Industrial Center, Detroit, Michigan  


## Education ## 

**Grand Valley State University**, Broadcasting. Dropped out.   
**Baker College**, Computer Programming  


## Darkroom ##

[![image](https://stevenbcherry.com/images/IMG_20170814_092322.jpg){:width="400px"}](https://stevenbcherry.com/galleries/darkroom/)


