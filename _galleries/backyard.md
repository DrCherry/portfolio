--- 
title: faded lux
layout: page
index: 60
camera: /cameras/pentax/ 
image: /images/09600013.jpg
description: Atmosphere and fading gold plating wears through to steel by hand. 
diaplay: true 
textcolor: light
images:
  - image: /images/09600013.jpg
  - image: /images/09600012.jpg
  - image: /images/09600015.jpg
  - image: /images/09600025.jpg
  - image: /images/09600009.jpg
--- 

{{ page.description }}
{% for item in page.images %}

![ {{ site.url }}{{ item.image }} ]( {{ site.url }}{{ item.image }} )

    
{% endfor %}


[ camera ]({{ site.url }}{{ page.camera }}){:class= .camera} 
