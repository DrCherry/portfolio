---
title: multiple-exposures
layout: page
camera: /cameras/ansco/
index: 70
display: true
image: /images/32040001.JPG
description: no forward, again, and again
textcolor: light
images:
 
  - image: /images/32040001.JPG
  - image: /images/32040002.JPG
  - image: /images/32040003.JPG
  - image: /images/32040006.JPG
  - image: /images/32040007.JPG
  - image: /images/75430015.jpg
  - image: /images/75430021.jpg
  - image: /images/75430015.jpg
---

 {{ page.description }}

{% for item in page.images %}

![ {{ site.url }}{{ item.image }} ]( {{ site.url }}{{ item.image }} )
    
{% endfor %}



