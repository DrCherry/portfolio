---
title: queenie
layout: page
camera: /cameras/pentax/
index: 30
display: true
image: /images/65560018.jpg 
description: The kitchen is warm in shadows. Say prayers, burn wood and coal.  Make hay, bake bread.  
textcolor: light
images:
  - image: /images/65560012.jpg 
  - image: /images/65560018.jpg
  - image: /images/65560031.jpg
  - image: /images/65570016.jpg
  - image: /images/65570017.jpg
  - image: /images/65570019.jpg
  - image: /images/65560008.jpg

---

 {{ page.description }}


{% for item in page.images %}

![ {{ site.url }}{{ item.image }} ]( {{ site.url }}{{ item.image }} )

    
{% endfor %}


[ camera ]({{ site.url }}{{ page.camera }}){:class= .camera } 


