---
title: at hart
display: true
layout: page
index: 80
camera: /cameras/pentax/
image: /images/31100003.jpg
description: Pentax SLR.  Hart, MI.
images:
  - image: /images/31100003.jpg
  - image: /images/31100007.jpg
  - image: /images/31100010.jpg
  - image: /images/31100024.jpg
  - image: /images/31100032-01.jpeg
  - image: /images/30850026.jpg
  - image: /images/30840021.jpg
  - image: /images/30840019.jpg
  - image: /images/30840003.jpg 
  - image: /images/30840004.jpg
  - image: /images/30840006.jpg
  - image: /images/IMG_20190331_212057_922.jpg
  - image: /images/IMG_20190331_204559_846.jpg
  - image: /images/IMG_20190331_210553_837.jpg
  - image: /images/30860031.jpg
  - image: /images/30860036.jpg
---

{% for item in page.images %}

![ {{ site.url }}{{ item.image }} ]( {{ site.url }}{{ item.image }} )

    
{% endfor %}

[ camera ]({{ site.url }}{{ page.camera }}){:class= .camera }

