---
title: streets and alleys
layout: page
camera: /cameras/zorki4/ 
index: 60
display: true
image: /images/000213220019.jpg
description: Streets are where we carry our knives and fight.
textcolor: dark
images:

  - image: /images/000213220019.jpg
  - image: /images/31880018.JPG
  - image: /images/000213220011.jpg
  - image: /images/17740033_11800_lrg_1457029045.jpg
  - image: /images/01680017.jpg
---

 {{ page.description }}


{% for item in page.images %}

![ {{ site.url }}{{ item.image }} ]( {{ site.url }}{{ item.image }} )

    
{% endfor %}


[ camera ]({{ site.url }}{{ page.camera }}){:class= .camera } 


