---
title: junkstore kodak
index: 10 
layout: page
image: /images/37430001.jpg 
camera: /cameras/kodak-bullet/
description: Walter Dorwin Teague, 127, light leaks, a night to remember.  Yielding.
textcolor: light
images:
  - image: /images/37430001.jpg
  - image: /images/37430002.jpg
  - image: /images/37430003.jpg
  - image: /images/37430004.jpg
  - image: /images/37430005.jpg
  - image: /images/37430006.jpg
  - image: /images/37430007.jpg  
---

{{ page.description }}

{% for item in page.images %}

![ {{ site.url }}{{ item.image }} ]( {{ site.url }}{{ item.image }} )

    
{% endfor %}

[ camera ]({{ site.url }}{{ page.camera }}){:class= .camera} 


