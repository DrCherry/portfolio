---
title: darkroom
layout: page
index: 700
display: false
image: /images/IMG_20170814_092322.jpg
description: This is where I repeat the process into mornings, silver in the washtub and then rinse the paper the clean.
textcolor: light
images:
  - image: /images/IMG_20171024_004744639.jpg
  - image: /images/IMG_20171024_004800426.jpg
  - image: /images/IMG_20170801_181944.jpg
  - image: /images/IMG_20170802_235820.jpg
  - image: /images/IMG_20170814_092322.jpg
  - image: /images/IMG_20171007_213928731.jpg
  - image: /images/IMG_20171023_213625789.jpg
  - image: /images/IMG_20171023_214420_866.jpg
  - image: /images/received_10212344617383101.jpg

---

 {{ page.description }}
 
{% for item in page.images %}

![ {{ site.url }}{{ item.image }} ]( {{ site.url }}{{ item.image }} )

    
{% endfor %}

