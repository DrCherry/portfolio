---
title: machine
layout: page
camera: /cameras/ansco/
index: 10
display: true
image: /images/000378170005.jpg
description: There are old machines in my neighborhood.
textcolor: light
images:
 
  - image: /images/000378170002.jpg
  - image: /images/000378170003.jpg
  - image: /images/000378170004.jpg
  - image: /images/000378170005.jpg
  - image: /images/000378170006.jpg
  - image: /images/000378170007.jpg
---

 {{ page.description }}


{% for item in page.images %}

![ {{ site.url }}{{ item.image }} ]( {{ site.url }}{{ item.image }} )

    
{% endfor %}


[ camera ]({{ site.url }}{{ page.camera }}){:class= .camera } 


