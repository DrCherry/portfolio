---
title: domestics / transport
layout: page
index: 70
camera: /cameras/minolta/
image: /images/04140005.jpg
description:  When northlight weds a spring afternoon, there is the constant struggle of men and dust.
textcolor: light
images:
  - image: /images/04140005.jpg
  - image: /images/04140008.jpg
  - image: /images/04140010.jpg
  - image: /images/04140011.jpg
  - image: /images/04140013.jpg
  - image: /images/04140016.jpg
  - image: /images/04140017.jpg
  - image: /images/04140018.jpg

---
{{ page.description }}
{% for item in page.images %}

![ {{ site.url }}{{ item.image }} ]( {{ site.url }}{{ item.image }} )

    
{% endfor %}


[ camera ]({{ site.url }}{{ page.camera }}){:class= .camera} 
