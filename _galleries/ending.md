---
title: endings
layout: page
camera: /cameras/time/
index: 70
display: true
image: /images/75720007.jpg
description: Nothing is perfect or free. Endings wander around town forgetting the warmth of their beds. Instead the comfort of the best casket they can afford.  
textcolor: light
images:
 
  - image: /images/75720007.jpg
  - image: /images/75720013.jpg
  - image: /images/75720015.jpg
  - image: /images/75720016.jpg
  - image: /images/75720017.jpg
  - image: /images/75720021.jpg
  - image: /images/75720023.jpg

---

 {{ page.description }}


{% for item in page.images %}

![ {{ site.url }}{{ item.image }} ]( {{ site.url }}{{ item.image }} )

    
{% endfor %}


[ camera ]({{ site.url }}{{ page.camera }}){:class= .camera } 


