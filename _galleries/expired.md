---
title: expired 1969
layout: page
camera: /cameras/ansco/ 
index: 60
display: true
image: /images/76050001.jpg
description: Expired in 1969, old stock, dirty, primative, rusted, and new. 
textcolor: light
images:

  - image: /images/76040003.jpg
  - image: /images/76040004.jpg
  - image: /images/76040005.jpg
  - image: /images/76040006.jpg
  - image: /images/76040007.jpg
  - image: /images/76050001.jpg
  - image: /images/76050002.jpg
  - image: /images/76050005.jpg
  - image: /images/76050006.jpg
  - image: /images/76050007.jpg

---

 {{ page.description }}


{% for item in page.images %}

![ {{ site.url }}{{ item.image }} ]( {{ site.url }}{{ item.image }} )

    
{% endfor %}


[ camera ]({{ site.url }}{{ page.camera }}){:class= .camera } 


