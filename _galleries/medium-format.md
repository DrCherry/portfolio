---
title: medium format
layout: page
index: 20
display: false
image: /images/IMG_20180106_211723_532.jpg
description: Arax 60 6x6, Agfa 6x6, Ansco Pioneer 6x9.
textcolor: light
images:
  - image: /images/IMG_20180106_211723_532.jpg
  - image: /images/IMG_20180213_134859_014.jpg
  - image: /images/IMG_20180228_000729_373.jpg
---

{% for item in page.images %}

![ {{ site.url }}{{ site.baseurl }}{{ item.image }} ]( {{ site.url }}{{ site.baseurl }}{{ item.image }} )

    
{% endfor %}

